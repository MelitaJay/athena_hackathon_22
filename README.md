# Virtual Forest


Welcome to the Virtual Forest!

This project was created by Gale Rate, Alice Ridgway and Melita Carty for the Athena Hackathon 2022.

The theme we chose was "How can households reduce their carbon footprint?"

We decided to build a Django web application that allows households to track their gas and electricity usage and have the results display in a gamified forest.

The intention is that this would encourage families with children to learn how to save energy and thus reduce their carbon footprint in a fun way.

The better the reduction of their average energy usage, the more quickly their forest will grow.

Note that this repository contains 2 Django apps - one for the Virtual Forest app itself, and one for the Energy company app (a separate application which the Virtual Forest app would perform API requests to).
<hr>

<h3>Dev Notes:</h3>

Includes:
- Custom user model
- User registration
- Linting (flake8 / mypy)

## How to set up

1. Create a virtual environment

```
python3 -m venv venv
```

2. Rename `djangoproject/.env.sample`, `djangoproject/.env`. Change the secret key to a value of your choice.

3. Install requirements
```
pip install -r requirements.txt
```

4. Run migrations
```
python manage.py migrate
```

5. Create a superuser
```
python manage.py createsuperuser
```

6. Enable linting (VScode only)

Create a directory called `.vscode`

In `.vscode`, create a file called `settings.json`

Copy the contents of `settings.json.sample` into `settings.json`.

This will enable Flake8, Mypy and Black. It will also enable autoformatting of Python files on-save as per the Black specification.
