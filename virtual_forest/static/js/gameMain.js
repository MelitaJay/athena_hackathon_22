/* Install canvas and game */
var canvasX = JSON.parse(document.getElementById("canvasWidth").textContent);
var canvasY = JSON.parse(document.getElementById("canvasHeight").textContent);

const game = new Phaser.Game(canvasX, canvasY, Phaser.CANVAS, "virtual-forest-canvas", {
    preload,
    create,
    update,
});
let tree;

function preload() {
    /* Canvas scale and colour */
    game.scale.scaleMode = Phaser.ScaleManager.RESIZE;
    game.scale.pageAlignHorizontally = true;
    game.scale.pageAlignVertically = true;
    game.stage.backgroundColor = "#a1a991";
    /* Load assets */
    game.load.image("tree1", "../static/img/tree1.png");
    game.load.image("tree2", "../static/img/tree2.png");
    game.load.image("tree3", "../static/img/tree3.png");
    game.load.image("tree4", "../static/img/tree4.png");
    game.load.image("bush1", "../static/img/bush1.png");
    game.load.image("bush2", "../static/img/bush2.png");
    game.load.image("flowers1", "../static/img/flowers1.png");
    game.load.image("flowers2", "../static/img/flowers2.png");
    game.load.spritesheet("deer", "../static/img/deer_spritesheet.png", 45, 30, 2);
    game.load.spritesheet("fox", "../static/img/fox_spritesheet.png", 47, 30, 2);
    game.load.spritesheet("blackbird", "../static/img/blackbird_spritesheet.png", 37, 30, 2);
}

function create() {
    initForest();
}

function update() { }

function initForest() {
    /* Include the trees and other forest objects on the canvas, using saved values. */

    var forestLayout = JSON.parse(document.getElementById("forestLayout").textContent);
    forestLayout = JSON.parse(forestLayout)
    // Need double JSON parse, potentially due to django context.

    plotObject(forestLayout)
}

function plotObject(forestLayout) {
    /* Convert forest layout coordinates and objects to plotted sprites */

    animals = ["deer", "fox", "blackbird"]

    for (let i = 0; i < forestLayout.length; i++) {
        layoutObject = forestLayout[i]
        if (animals.includes(layoutObject.object_type)) {
            /* Add sprite animation */
            gameSprite = game.add.sprite(layoutObject.object_x, layoutObject.object_y, layoutObject.object_type, 2);
            spriteName = layoutObject.object_type.concat("Animated")
            animated = gameSprite.animations.add(spriteName)
            animated.play(game.rnd.frac(), true)
        }
        else {
            gameSprite = game.add.sprite(layoutObject.object_x, layoutObject.object_y, layoutObject.object_type);
        }
    }
}
