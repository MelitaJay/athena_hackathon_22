"""
Wrapper around calls to the energy company API.
You will need to run the API locally on port 8031

cd energy_company_api
python manage.py runserver 8031

"""

import json
import requests

from .constants import EnergyType

BASE_URL = "http://localhost:8031/api/v1/"


def get_growth_factor(username: str, reading_type: str) -> dict:

    permitted_types = [EnergyType.ELECTRICITY, EnergyType.GAS]
    if reading_type not in permitted_types:
        raise ValueError(
            f"{reading_type} is not a valid type. Expected one of {permitted_types}."
        )

    url = f"{BASE_URL}{username}/{reading_type}/get_growth_factor"

    response = requests.get(url)

    data = json.loads(response.content)

    if data["code"] != 200:
        raise Exception("Something went wrong.")

    return data
