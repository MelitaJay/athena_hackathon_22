from django.urls import path

from core import views

urlpatterns = [
    path("", views.index, name="index"),
    path("utilities", views.utilities, name="utilities"),
    path("utilities/<str:utility_name>", views.utility, name="utility"),
    path("sponsors", views.sponsors, name="sponsors"),
    path("add_trees", views.add_trees, name="add_trees"),
]
