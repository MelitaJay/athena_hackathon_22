import json
import random
import math

from core.models import ForestLayout, TreeSyncPoint
from core.model_utils import get_utility_growth_factor_for_profile, add_trees_to_profile
from datetime import datetime


def generate_object_position_dictionary(object_type, maximum_x_pos, maximum_y_pos):
    """Generate positions on the canvas for new objects"""

    position_dict = {
        "object_type": object_type,
        "object_x": random.randint(1, maximum_x_pos),
        "object_y": random.randint(1, maximum_y_pos),
    }

    return position_dict


def convert_growth_rate_to_trees(growth_rate):
    """Convert growth rate to the increased number of trees"""

    fraction_reduction = 1 - growth_rate

    if fraction_reduction < 0.011:
        # Stop negative values from being returned.
        number_of_trees_add = 0
    else:
        number_of_trees_add = 1.63 * math.log(fraction_reduction) + 6.88
        # Use natural logarithm to convert growth rate to trees.

    number_of_trees_add = round(number_of_trees_add)
    # Number of trees can only be an integer.

    return number_of_trees_add


def add_woodland_creature(
    forest_layout, maximum_x_pos, maximum_y_pos, available_tree_options
):
    """Chooses to add a woodland creature if number of trees is high enough"""

    number_of_trees_total = sum(
        [1 if i["object_type"] in available_tree_options else 0 for i in forest_layout]
    )

    deer_in_forest_layout = sum(
        [1 if i["object_type"] == "deer" else 0 for i in forest_layout]
    )

    fox_in_forest_layout = sum(
        [1 if i["object_type"] == "fox" else 0 for i in forest_layout]
    )

    blackbird_in_forest_layout = sum(
        [1 if i["object_type"] == "blackbird" else 0 for i in forest_layout]
    )

    if (number_of_trees_total > (6 * (deer_in_forest_layout + 1))) and (
        deer_in_forest_layout < 10
    ):
        # Generate deer every 6 trees.
        output_object = generate_object_position_dictionary(
            "deer", maximum_x_pos, maximum_y_pos
        )
    elif (number_of_trees_total > (10 * (fox_in_forest_layout + 1))) and (
        fox_in_forest_layout < 10
    ):
        # Generate fox every 10 trees.
        output_object = generate_object_position_dictionary(
            "fox", maximum_x_pos, maximum_y_pos
        )
    elif (number_of_trees_total > (8 * (blackbird_in_forest_layout + 1))) and (
        blackbird_in_forest_layout < 10
    ):
        # Generate blackbird every 8 trees
        output_object = generate_object_position_dictionary(
            "blackbird", maximum_x_pos, maximum_y_pos
        )
    else:
        output_object = None

    return output_object


def add_bushes_or_flowers(maximum_x_pos, maximum_y_pos):
    """Add some random bushes or flowers to fill the space"""

    available_objects = ["bush1", "bush2", "flowers1", "flowers2"]
    add_object_type = random.choice(available_objects)
    random_add = random.randint(0, 10)

    if random_add < 7:
        return generate_object_position_dictionary(
            add_object_type, maximum_x_pos, maximum_y_pos
        )
    else:
        return None


def add_all_objects_to_json(
    existing_positions, add_trees_number, maximum_x_pos, maximum_y_pos
):
    """Add all trees, woodland creatures and decoration to the json"""

    available_tree_options = ["tree1", "tree2", "tree3", "tree4"]

    for i in range(0, add_trees_number):
        tree_type = random.choice(available_tree_options)
        position = generate_object_position_dictionary(
            tree_type, maximum_x_pos, maximum_y_pos
        )
        existing_positions.forest_layout.append(position)

    # Add forest creature.
    add_creature = add_woodland_creature(
        existing_positions.forest_layout,
        maximum_x_pos,
        maximum_y_pos,
        available_tree_options,
    )
    if add_creature:
        existing_positions.forest_layout.append(add_creature)

    add_forest_decoration = add_bushes_or_flowers(maximum_x_pos, maximum_y_pos)
    if add_forest_decoration:
        existing_positions.forest_layout.append(add_forest_decoration)

    # Sort to ensure no overlap and then save.
    existing_positions.forest_layout = sorted(
        existing_positions.forest_layout, key=lambda l: l["object_y"]
    )
    existing_positions.save()


def initial_layout(initial_trees, initial_type, profile, maximum_x_pos, maximum_y_pos):
    """Create an initial forest layout"""

    generated_list = []
    for i in range(0, initial_trees):
        position = generate_object_position_dictionary(
            initial_type, maximum_x_pos, maximum_y_pos
        )
        generated_list.append(position)

    new_layout = ForestLayout(profile=profile, forest_layout=generated_list)
    new_layout.forest_layout = sorted(
        new_layout.forest_layout, key=lambda l: l["object_y"]
    )
    # Sort based on y position (to stop overlapping placement.)
    new_layout.save()


def last_sync_point_today(profile):
    """Check updated tree generation if there was a tree sync point today"""

    # uncomment to fake new days
    # return False

    tree_syncs = TreeSyncPoint.objects.filter(profile=profile)
    last_tree_sync = tree_syncs.last()
    if not last_tree_sync:
        return False

    last_tree_sync_date = last_tree_sync.date.strftime("%Y-%m-%d")
    todays_date = datetime.today().strftime("%Y-%m-%d")
    tree_sync_today = last_tree_sync_date == todays_date

    return tree_sync_today


def all_objects_positions(
    profile,
    utility,
    maximum_x_pos,
    maximum_y_pos,
    initial_trees=5,
    initial_type="tree1",
):
    """Get positions of existing objects and generate positions for any new objects."""
    growth_rate = get_utility_growth_factor_for_profile(profile, utility)

    existing_positions = ForestLayout.objects.filter(profile=profile).first()
    # Should only be one element per profile.

    if not existing_positions:
        initial_layout(
            initial_trees, initial_type, profile, maximum_x_pos, maximum_y_pos
        )
        # Generate an initial layout.
        existing_positions = ForestLayout.objects.filter(profile=profile).first()
        add_trees_to_profile(profile, initial_trees)
        # Get the updated layout.
    else:
        # Generate additional elements.
        add_trees_number = convert_growth_rate_to_trees(growth_rate)

        # Add new trees:
        if add_trees_number and not last_sync_point_today(profile):
            add_trees_to_profile(profile, add_trees_number)
            # Add to profile tree count.
            add_all_objects_to_json(
                existing_positions, add_trees_number, maximum_x_pos, maximum_y_pos
            )

    forest_layout_json = json.dumps(existing_positions.forest_layout)

    return forest_layout_json
