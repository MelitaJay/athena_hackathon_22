import random

ENERGY_SAVING_TIPS = {
    1: "Cutting just a minute off your shower time could save £207 a year in energy bills",
    2: "You can save £16 per year by not over-filling the kettle",
    3: "The average household spends £117 per year running the tumbledryer. Try hanging your clothes to dry instead.",
    4: "Slow cookers cost 8p per hour to run on average, vs 68p per hour for an oven.",
    5: "Keep the heat in! Use a rolled up blanket to keep draughts out.",
    6: "Turning down your themostat by just 1 degree can save you up to £128 per year and reduce your carbon footprint",
    7: "Devices left on standby waste electricity- turn them off to save money and emissions",
    8: "Saving energy will reduce carbon dioxide emissions, which is good for the planet",
    9: "Challenge! Find out how many watts your lightbulbs use.",
    10: "Washing your clothes at 30 degrees can save you up to £54 per year.",
}


def get_random_energy_saving_tip() -> str:

    number_of_tips = len(ENERGY_SAVING_TIPS.keys())

    index = random.randint(1, number_of_tips)

    return ENERGY_SAVING_TIPS[index]
