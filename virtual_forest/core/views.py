import json

from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import LoginView
from django.http import HttpRequest, HttpResponse
from django.shortcuts import render
from jsonview.decorators import json_view


from core.model_utils import (
    add_trees_to_profile,
    get_number_of_trees_for_profile,
    get_profile_for_user,
    get_user_name_for_profile,
    get_utilities,
    get_utilities_data,
    get_utility,
    get_utility_data,
    get_utility_growth_factor_for_profile,
    save_utility_growth_factor_for_profile,
    user_has_logged_in_today,
    get_forest_layout_for_profile,
)
from core.utils import all_objects_positions, add_all_objects_to_json
from core.energy_saving_tips import get_random_energy_saving_tip

from energy_company_api.get_requests import get_growth_factor


class CanvasData:
    """
    Class to hold canvas data.
    """

    canvas_width = 1000
    canvas_height = 820
    padding = 20

    object_position_x = canvas_width - padding
    object_position_y = canvas_height - padding


def pre_login(request: HttpRequest, user):
    """
    Do some stuff before we redirect the user to the homepage
    """

    user_not_logged_in_today = not user_has_logged_in_today(user)

    profile = get_profile_for_user(user)
    # If this is the first time the user has logged in today,
    if user_not_logged_in_today:

        number_of_trees = 10

        # give them some bonus trees
        add_trees_to_profile(profile, number_of_trees)
        existing_positions = get_forest_layout_for_profile(profile)
        add_all_objects_to_json(
            existing_positions,
            number_of_trees,
            CanvasData.object_position_x,
            CanvasData.object_position_y,
        )

    for utility in get_utilities():
        if user_not_logged_in_today or not get_utility_growth_factor_for_profile(
            profile, utility
        ):
            # save their growth rate
            save_utility_growth_factor_for_profile(profile, utility)


class CustomLoginView(LoginView):
    def form_valid(self, form):
        """Security check complete. Log the user in."""

        pre_login(self.request, form.get_user())
        ret = super().form_valid(form)

        return ret


@login_required
def index(request: HttpRequest) -> HttpResponse:

    profile = get_profile_for_user(request.user)
    utility_to_use = get_utilities().last()

    forest_layout = all_objects_positions(
        profile,
        utility_to_use,
        CanvasData.object_position_x,
        CanvasData.object_position_y,
    )

    user_name = get_user_name_for_profile(profile)
    tree_count = get_number_of_trees_for_profile(profile)

    context = {
        "user_name": user_name,
        "tree_points": tree_count,
        "forest_layout": forest_layout,
        "canvas_width": CanvasData.canvas_width,
        "canvas_height": CanvasData.canvas_height,
        "energy_saving_tip": get_random_energy_saving_tip(),
    }

    return render(request, "index.html", context)


@login_required
def utilities(request: HttpRequest) -> HttpResponse:

    utilities = get_utilities_data()

    context = {
        "title": "Utilities",
        "utilities": utilities,
        "sidebar_active": "utilities",
    }

    return render(request, "utilities.html", context)


@login_required
def utility(request: HttpRequest, utility_name: str) -> HttpResponse:

    profile = get_profile_for_user(request.user)
    utility = get_utility(utility_name)

    data = get_utility_data(profile, utility)

    context = {"title": utility, **data}

    return render(request, "utility.html", context)


@login_required
def sponsors(request: HttpRequest) -> HttpResponse:

    sponsor_posts = [
        {
            "title": "10% off",
            "description": "Use our code to get 10% off",
            "sponsor": "Sponsor 1",
            "banner": "10% off",
            "image": "img/sponsor_1.jpg",
        },
        {
            "title": "Free delivery",
            "description": "Get free UK delivery on your first order",
            "sponsor": "Sponsor 2",
            "banner": "Free delivery",
            "image": "img/sponsor_2.jpg",
        },
        {
            "title": "£15 off",
            "description": "£15 when you spend £50 or more",
            "sponsor": "Sponsor 3",
            "banner": "£15 off",
            "image": "img/sponsor_3.jpg",
        },
    ]

    context = {
        "sponsor_posts": sponsor_posts,
        "sidebar_active": "sponsors",
    }

    return render(request, "sponsors.html", context)


@login_required
@json_view
def add_trees(request: HttpRequest) -> dict:

    data = json.loads(request.body.decode("utf-8"))

    number_of_trees = data.get("number_of_trees")

    profile = get_profile_for_user(request.user)

    add_trees_to_profile(profile, number_of_trees)

    existing_positions = get_forest_layout_for_profile(profile)
    add_all_objects_to_json(
        existing_positions,
        number_of_trees,
        CanvasData.object_position_x,
        CanvasData.object_position_y,
    )

    return {
        "success": True,
    }
