# Generated by Django 4.1.2 on 2022-10-22 19:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("core", "0003_alter_profile_user_alter_utility_name_and_more"),
    ]

    operations = [
        migrations.AddField(
            model_name="utilitydelta",
            name="end_date",
            field=models.DateTimeField(null=True),
        ),
        migrations.AddField(
            model_name="utilitydelta",
            name="start_date",
            field=models.DateTimeField(null=True),
        ),
    ]
