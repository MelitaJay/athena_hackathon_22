# Generated by Django 4.1.2 on 2022-10-22 19:35

from django.db import migrations

from datetime import datetime


def forwards(apps, schema_editor):

    UtilityDelta = apps.get_model("core", "UtilityDelta")
    Utility = apps.get_model("core", "Utility")
    Profile = apps.get_model("core", "Profile")

    profile = Profile.objects.first()
    utilities = Utility.objects.all()

    UtilityDelta.objects.bulk_create(
        [
            UtilityDelta(
                profile=profile,
                utility=utility,
                delta=len(utility.name) * 10,
                start_date=datetime.strptime("2022-10-01", "%Y-%m-%d"),
                end_date=datetime.now(),
            )
            for utility in utilities
        ]
    )


class Migration(migrations.Migration):

    dependencies = [
        ("core", "0004_utilitydelta_end_date_utilitydelta_start_date"),
    ]

    operations = [
        migrations.RunPython(
            forwards,
            reverse_code=migrations.RunPython.noop,
        )
    ]
