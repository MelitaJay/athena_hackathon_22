# Generated by Django 4.1.2 on 2022-10-23 08:49

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("core", "0004_add_forest_layout_to_profile"),
        ("core", "0005_auto_20221022_1935"),
    ]

    operations: list = []
