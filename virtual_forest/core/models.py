from django.conf import settings
from django.db import models

from accounts.models import CustomUser

from django.dispatch import receiver  # add this
from django.db.models.signals import post_save  # add this


class Profile(models.Model):
    """User Profile"""

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE, blank=False, unique=True
    )

    def __str__(self) -> str:
        return self.user.username

    @receiver(post_save, sender=CustomUser)  # add this
    def create_user_profile(sender, instance, created, **kwargs):
        if created:
            Profile.objects.create(user=instance)


class Utility(models.Model):
    """E.g. gas, electricity"""

    name = models.CharField(max_length=64, unique=True)
    conversion_rate = models.PositiveIntegerField(db_index=True)

    def __str__(self) -> str:
        return self.name


class GrowthFactor(models.Model):

    utility = models.ForeignKey(Utility, on_delete=models.CASCADE)
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE)
    value = models.DecimalField(max_digits=15, decimal_places=2, default=0)

    class Meta:
        unique_together = ("utility", "profile")

    def __str__(self) -> str:
        return f"{self.profile} - {self.utility} - {self.value}"


class UtilityDelta(models.Model):
    """A delta storing the units of utility used for a certain number of days"""

    delta = models.PositiveIntegerField(db_index=True)
    utility = models.ForeignKey(Utility, on_delete=models.CASCADE, default=None)
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE, default=None)

    start_date = models.DateTimeField(null=True)
    end_date = models.DateTimeField(null=True)


class TreeSyncPoint(models.Model):
    """How many trees the profile has on a given date"""

    profile = models.ForeignKey(Profile, on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now_add=True)
    number_of_trees = models.PositiveIntegerField(db_index=True, default=0)

    def __str__(self) -> str:
        return f"{self.profile} - {self.date.date()}: {self.number_of_trees}"


class ForestLayout(models.Model):
    """The current forest layout for the profile"""

    profile = models.ForeignKey(Profile, on_delete=models.CASCADE)
    forest_layout = models.JSONField(db_index=True, default=None, null=True, blank=True)

    def __str__(self) -> str:
        return str(self.profile)
