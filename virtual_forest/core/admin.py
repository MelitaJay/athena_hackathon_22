from django.contrib import admin

from .models import (
    Profile,
    Utility,
    UtilityDelta,
    TreeSyncPoint,
    ForestLayout,
    GrowthFactor,
)


class ProfileAdmin(admin.ModelAdmin):
    pass


class UtilityAdmin(admin.ModelAdmin):
    pass


class GrowthFactorAdmin(admin.ModelAdmin):
    pass


class UtilityDeltaAdmin(admin.ModelAdmin):
    pass


class TreeSyncPointAdmin(admin.ModelAdmin):
    pass


class ForestLayoutAdmin(admin.ModelAdmin):
    pass


admin.site.register(Profile, ProfileAdmin)
admin.site.register(Utility, UtilityAdmin)
admin.site.register(GrowthFactor, GrowthFactorAdmin)
admin.site.register(UtilityDelta, UtilityDeltaAdmin)
admin.site.register(TreeSyncPoint, TreeSyncPointAdmin)
admin.site.register(ForestLayout, ForestLayoutAdmin)
