from datetime import datetime

from .models import (
    Utility,
    TreeSyncPoint,
    Profile,
    UtilityDelta,
    GrowthFactor,
    ForestLayout,
)

from energy_company_api.get_requests import get_growth_factor


def get_profile_for_user(user):

    profile, _ = Profile.objects.get_or_create(user=user)
    return profile


def get_utility(name: str):

    return Utility.objects.get(name__iexact=name)


def get_utilities():

    return Utility.objects.all()


def get_utilities_data():

    utilities = Utility.objects.all()
    return [
        {
            "name": utility.name,
            "image": f"img/{utility.name.lower()}.jpg",
            "link": utility.name.lower(),
        }
        for utility in utilities
    ]


def format_as_percentage_increase(growth_factor: float) -> str:

    pc_change = abs(1 - growth_factor) * 100

    suffix = "increase" if growth_factor > 1 else "decrease"

    return f"{round(pc_change, 1)}% {suffix}"


def get_utility_data(profile: Profile, utility: Utility):

    start_date = None
    end_date = None

    utility_delta = UtilityDelta.objects.filter(profile=profile, utility=utility)

    if utility_delta:

        (start_date, end_date) = utility_delta.values_list(
            "start_date", "end_date"
        ).last()

    data = get_growth_factor(profile.user.username, utility.name.lower())

    usage_change = format_as_percentage_increase(data["growth"])

    return {
        "start_date": start_date,
        "end_date": end_date,
        "average_units__3": round(data["units_per_day__3"], 2),
        "average_units__90": round(data["units_per_day__90"], 2),
        "growth_factor": usage_change,
    }


def save_utility_growth_factor_for_profile(profile: Profile, utility: Utility):
    """Return the growth factor for a given user profile"""

    user_name = str(profile.user)

    data = get_growth_factor(user_name.lower(), str(utility).lower())
    growth_factor_value = data["growth"]

    growth_factor, _ = GrowthFactor.objects.update_or_create(
        profile=profile, utility=utility, defaults={"value": growth_factor_value}
    )


def get_number_of_trees_for_profile(profile: Profile):

    return (
        TreeSyncPoint.objects.filter(profile=profile)
        .order_by(
            "-date",
        )
        .values_list("number_of_trees", flat=True)
    ).first() or 0


def get_utility_growth_factor_for_profile(profile: Profile, utility: Utility):

    return (
        GrowthFactor.objects.filter(profile=profile, utility=utility)
        .values_list("value", flat=True)
        .first()
        or 0
    )


def get_user_name_for_profile(profile: Profile):

    return profile.user.username


def user_has_logged_in_today(user) -> bool:

    if not user.last_login:
        return False

    last_login = user.last_login

    today = datetime.today()

    return last_login.date() == today.date()


def _create_tree_sync_point(profile: Profile, number_of_trees: int):
    """
    Create a tree sync point when the user logs in
    """

    date = datetime.today()

    tree_sync_point = TreeSyncPoint.objects.create(
        date=date, profile=profile, number_of_trees=number_of_trees
    )

    return tree_sync_point


def add_trees_to_profile(profile: Profile, number_of_trees: int):

    current_trees = get_number_of_trees_for_profile(profile)

    new_trees = current_trees + number_of_trees

    _create_tree_sync_point(profile, new_trees)


def get_forest_layout_for_profile(profile: Profile):

    forest_layout = ForestLayout.objects.filter(profile=profile).first()

    if forest_layout is None:
        forest_layout = ForestLayout.objects.create(profile=profile, forest_layout=[])

    return forest_layout
