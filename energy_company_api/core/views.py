from django.http import HttpRequest, HttpResponse
from django.shortcuts import render
import io, urllib, base64
from matplotlib import pyplot as plt

# Create your views here.

from core import unit_calculations
from core.models import MeterReading


def index(request: HttpRequest) -> HttpResponse:

    context = {"message": "Hello World"}

    return render(request, "index.html", context)


def get_chart(request: HttpRequest, username: str, utility: str) -> HttpResponse:

    meter_readings = MeterReading.objects.filter(
        user__username=username, reading_type=utility
    ).order_by("date")

    average_units__90 = unit_calculations.get_usage_for_last_n_days(90, meter_readings)
    average_units__3 = unit_calculations.get_usage_for_last_n_days(3, meter_readings)

    readings = meter_readings.values_list("reading", flat=True)
    last_90_days = readings.count() - 90
    last_10_days = readings.count() - 10
    plt.clf()
    plt.subplot(1, 2, 1, title="Meter Readings - Last 90 days")
    plt.plot(readings[last_90_days:])

    plt.subplot(1, 2, 2, title="Meter Readings - Last 10 days")
    plt.plot(readings[last_10_days:])

    fig = plt.gcf()
    # convert graph into dtring buffer and then we convert 64 bit code into image
    buf = io.BytesIO()
    fig.savefig(buf, format="png")
    buf.seek(0)
    string = base64.b64encode(buf.read())
    uri = urllib.parse.quote(string)

    return render(
        request,
        "chart.html",
        {
            "username": username.capitalize(),
            "utility": utility.capitalize(),
            "data": uri,
            "units_per_day__90": average_units__90,
            "units_per_day__3": average_units__3,
            "growth": unit_calculations.get_growth(average_units__90, average_units__3),
        },
    )
