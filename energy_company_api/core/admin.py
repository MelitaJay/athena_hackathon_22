from django.contrib import admin

from core.models import MeterReading

# Register your models here.


class MeterReadingAdmin(admin.ModelAdmin):

    readonly_fields = ["date"]


admin.site.register(MeterReading, MeterReadingAdmin)
