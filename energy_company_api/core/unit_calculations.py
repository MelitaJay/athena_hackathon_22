from core.models import MeterReading
from django.db.models import QuerySet


def get_average_units_per_day(
    reading_a: MeterReading, reading_b: MeterReading
) -> float:
    """
    Gets the average units used per day between two meter readings

    """
    units_used = reading_a.reading - reading_b.reading

    days_between_readings = (reading_a.date - reading_b.date).days

    return units_used / days_between_readings


def get_usage_for_last_n_days(n: int, readings: QuerySet[MeterReading]):

    start_index = readings.count() - n

    start_reading = readings[start_index]
    end_reading = readings.latest("date")

    return round(get_average_units_per_day(end_reading, start_reading), 2)


def get_growth(average_units__long: float, average_units__short: float) -> float:

    return round(average_units__short / average_units__long, 2)
