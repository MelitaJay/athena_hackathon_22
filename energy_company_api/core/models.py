from django.db import models
from datetime import datetime
from accounts.models import CustomUser

# Create your models here.


class MeterReading(models.Model):
    class ReadingType(models.TextChoices):
        ELECTRICITY = "electricity"
        GAS = "gas"

    user = models.ForeignKey(
        to=CustomUser, on_delete=models.CASCADE, related_name="readings"
    )

    reading = models.PositiveIntegerField()
    reading_type = models.CharField(choices=ReadingType.choices, max_length=20)
    date = models.DateTimeField(default=datetime.now())

    def __str__(self):
        return f"{self.reading_type}: {self.date} for {self.user}"
