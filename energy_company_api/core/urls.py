from django.urls import path

from core import views

urlpatterns = [
    path("", views.index, name="index"),
    path("<str:username>/<str:utility>", views.get_chart, name="chart"),
]
