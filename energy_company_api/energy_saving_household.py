"""
The Carty household used to be an average household in terms of energy usage.

Last week, the Carty's installed solar panels, cutting their electricity usage in half. Gas readings have stayed the same.

This script mocks meter readings for the Carty household

"""
# exec(open('energy_saving_household.py').read())
import random
from datetime import datetime, timedelta

from accounts.models import CustomUser

from core.models import MeterReading


user = CustomUser.objects.get_or_create(username="carty")[0]

PERIOD = 120

START = datetime.now() - timedelta(days=PERIOD)

electricity_reading = 0
gas_reading = 0

meter_readings = []

for i in range(120):

    date = START + timedelta(days=i)
    print(date)

    electricity_usage = random.randint(4, 6) if i > 113 else random.randint(8, 12)

    electricity_reading = electricity_reading + electricity_usage

    gas_reading = gas_reading + random.randint(33, 38)

    print(electricity_reading, gas_reading)

    meter_readings.append(
        MeterReading(
            date=date,
            reading_type=MeterReading.ReadingType.ELECTRICITY,
            reading=electricity_reading,
            user=user,
        )
    )

    meter_readings.append(
        MeterReading(
            date=date,
            reading_type=MeterReading.ReadingType.GAS,
            reading=gas_reading,
            user=user,
        )
    )

MeterReading.objects.bulk_create(meter_readings)
