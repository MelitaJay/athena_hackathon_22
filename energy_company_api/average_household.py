# exec(open('average_household.py').read())
import random
from datetime import datetime, timedelta

from django.core.wsgi import get_wsgi_application  # noqa

application = get_wsgi_application()

from accounts.models import CustomUser
from core.models import MeterReading

user = CustomUser.objects.get_or_create(username="ridgway")[0]

PERIOD = 120

START = datetime.now() - timedelta(days=PERIOD)

electricity_reading = 0
gas_reading = 0
meter_readings = []

for i in range(120):

    date = START + timedelta(days=i)
    print(date)
    electricity_reading = electricity_reading + random.randint(8, 12)
    gas_reading = gas_reading + random.randint(33, 38)

    print(electricity_reading, gas_reading)

    meter_readings.append(
        MeterReading(
            date=date,
            reading_type=MeterReading.ReadingType.ELECTRICITY,
            reading=electricity_reading,
            user=user,
        )
    )

    meter_readings.append(
        MeterReading(
            date=date,
            reading_type=MeterReading.ReadingType.GAS,
            reading=gas_reading,
            user=user,
        )
    )

MeterReading.objects.bulk_create(meter_readings)
