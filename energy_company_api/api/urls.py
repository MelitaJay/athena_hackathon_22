from django.urls import path

from api import views

urlpatterns = [
    path(
        "<str:username>/<str:reading_type>/get_growth_factor",
        views.get_growth_factor,
        name="growth",
    ),
]
