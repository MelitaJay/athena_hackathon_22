import math
from django.http import HttpRequest
from datetime import datetime, timedelta

from django.http import JsonResponse, HttpResponseBadRequest, HttpResponseServerError

from core.models import MeterReading
from core import unit_calculations

from accounts.models import CustomUser

# Create your views here.


def get_growth_factor(
    request: HttpRequest, username: str, reading_type: str
) -> JsonResponse:
    """
    Returns a "growth" factor in meter readings.

    Compares readings over the past three days with readings over the past 90 days.

    Delta period: minimum 3 days
    Averaging period: minimum 90 days

    If growth > 1, then usage is growing
    If growth < 1, then usage is shrinking

    """
    try:
        user = CustomUser.objects.get(username=username)
    except CustomUser.DoesNotExist:
        return JsonResponse({"code": 400, "message": "Invalid User"})

    valid_reading_types = [choice[0] for choice in MeterReading.ReadingType.choices]

    if reading_type not in valid_reading_types:
        return JsonResponse(
            {
                "code": 400,
                "message": f"{reading_type} is not a valid utility. Expecting {valid_reading_types}",
            }
        )

    try:
        averaging_period_in_days = 90

        delta_period_in_days = 3  # delta must use minimum 3 days of data

        start_of_averaging_period = datetime.now() - timedelta(
            days=averaging_period_in_days
        )

        readings = MeterReading.objects.filter(
            user=user,
            reading_type=reading_type,
        ).order_by("-date")

        latest_reading = readings.latest("date")

        try:
            earliest_in_averaging_period = readings.filter(
                date__lte=start_of_averaging_period
            ).latest("date")
        except MeterReading.DoesNotExist:
            earliest_in_averaging_period = readings.latest("date")

        units_per_day__averaging_period = unit_calculations.get_average_units_per_day(
            latest_reading, earliest_in_averaging_period
        )

        start_of_delta_period = latest_reading.date - timedelta(delta_period_in_days)

        earliest_in_delta_period = readings.filter(
            date__lte=start_of_delta_period
        ).latest("date")

        units_per_day__delta_period = unit_calculations.get_average_units_per_day(
            latest_reading, earliest_in_delta_period
        )

        growth_factor = units_per_day__delta_period / units_per_day__averaging_period

        response = {
            "code": 200,
            "growth": growth_factor,
            "reading_type": reading_type,
            "username": username,
            "units_per_day__3": units_per_day__delta_period,
            "units_per_day__90": units_per_day__averaging_period,
        }

    except Exception as e:
        return JsonResponse(
            {"code": 500, "message": f"Input is valid but something's gone wrong: {e}"}
        )

    return JsonResponse(response)
